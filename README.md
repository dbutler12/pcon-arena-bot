# pcon-arena-bot


**********Datastructures**********

Defense or Offense Teams:
	Examples
		char1_char2_char3_char4_char5
		def_str or off_str example: Illya_Monika_Pecorine_Jun_Kuka


Redis Strings:
	Version Data
		cur_version:  2.5.0


Redis Sets:
	Player Arena Teams
		ba_teams_(player tag): stores set of teams arranged by (player team)-(challenged team)


Redis Sorted Sets:
	Character Appreciation
		love_(usertag): stores character names with appreciation values
		
	User Experience
		user_exp: stores tags with exp values
		
	Best Teams
		winning_teams: stores teams that have won in the !arena command
		
		
Redis Hash:
	Character Data
		char_data_(id): stores all pertinent character related data

	Unresolved Challenges
		challenges: stores (challenger tag)_(challenged tag) linked to defense team of challenger

	Character Nicknames
		char_nick: stores nickname linked to ID
		
	Current BA Teams
		ba_teams: stores user tag linked to next up defense team
