FROM alpine:latest

WORKDIR /usr/src/bot
COPY package.json .env ./

RUN apk add --update \
	&& apk add --no-cache nodejs-current npm \
	&& apk add --no-cache --virtual .build git curl build-base g++ \
	&& npm install \
	&& apk del .build

COPY . .

CMD [ "node", "src/arena-bot.js" ]
