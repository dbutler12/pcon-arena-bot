function getEmojiString(d_client, character){
	let str = d_client.emojis.cache.find(emoji => emoji.name === character);
	return (str == undefined) ? character : str.toString();
}

//TODO: Move this and its copy (from game.js) to a helper functions file
function generateRand(upTo, val1 = false, val2 = false){
	if((!val1 && !val2) || upTo == 1) return Math.floor(Math.random()*upTo);
	let count = 0;
	let val = Math.floor(Math.random()*upTo);

	while((val == val1 || val == val2) && count < 25) {
		val = Math.floor(Math.random()*upTo);
		count++;
	}
	return val;
}

//TODO: Refactor this function. I made it after work so it's a mess
async function getRandAltEmojiString(d_client, r_client, character){
	const { promisify } = require('util');
	const hashAsync = promisify(r_client.hgetall).bind(r_client);
	let alts = await hashAsync('char_alts');
	let nick = await hashAsync('char_nick');
	let alt_list = "";
	if(character in nick && nick[character] in alts){
		alt_list = alts[nick[character]];
	}
	alt_list = alt_list.split("_");
	let choice = generateRand(alt_list.length+1);
	if(choice == alt_list.length){
		console.log("MFK: Original selected.");
		return getEmojiString(d_client, character); // No alt this time!
	}
	let alt = alt_list[choice];
	if(alt != ""){
		let char_data = await hashAsync('char_data_' + alt);
		console.log("MFK:");
		console.log(char_data['emoji']);
		return getEmojiString(d_client, char_data['emoji']);
	}
	console.log("MFK: No alts found.");
	return getEmojiString(d_client,character);
}

async function extractCharStr(str){
	let c = str.charAt(0);
	if(c == '<' || c == ':'){ // The two current options for an emoji
		str = str.split(':')[1];
	}
	return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

module.exports = { getEmojiString, getRandAltEmojiString, extractCharStr };
