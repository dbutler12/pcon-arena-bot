function init(r_client){
	r_client.get('cur_char_id', function(err, reply){
		if(reply == null || reply == undefined){
			r_client.set('cur_char_id', '0');
		}
	});
}

module.exports = { init };
